require 'rails_helper'

RSpec.feature 'potepan::categories', type: :feature do
  given(:taxonomy)  { create(:taxonomy, name: 'cateagories') }
  given(:taxon1)    { create(:taxon, name: 'Bags', taxonomy: taxonomy) }
  given!(:taxon2)   { create(:taxon, name: 'Mugs', taxonomy: taxonomy) }
  given!(:product)  { create(:product, name: 'TOTE', price: '10', taxons: [taxon1]) }
  given!(:product2) { create(:product, name: 'Ruby', price: '20', taxons: [taxon2]) }

  background do
    visit potepan_category_path(taxon1.id)
  end

  scenario '正しくタイトルとサイドバーが表示される' do
    expect(page).to have_title "#{taxon1.name} - #{BASE_TITLE}"
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
      click_on taxonomy.name
      expect(page).to have_content "#{taxon1.name}(#{taxon1.products.length})"
      expect(page).to have_content "#{taxon2.name}(#{taxon2.products.length})"
    end
  end

  scenario '選択したカテゴリーに属する商品が表示され、属さない商品は表示されない' do
    click_on "#{taxon1.name}(#{taxon1.products.length})"
    expect(current_path).to eq potepan_category_path(taxon1.id)
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon1.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).not_to have_content product2.name
    expect(page).not_to have_content product2.display_price
  end

  scenario '商品名をクリックすると商品詳細ページに移動する' do
    expect(page).to have_link product.name
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end
end
