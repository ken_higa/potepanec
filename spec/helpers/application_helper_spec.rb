require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  describe 'full_title method test' do
    it 'return base_title' do
      expect(full_title("")).to eq BASE_TITLE
    end

    it 'return full_title provide example title' do
      expect(full_title("example title")).to eq "example title - #{BASE_TITLE}"
    end

    it 'return base_title case nil' do
      expect(full_title(nil)).to eq BASE_TITLE
    end
  end
end
