require 'rails_helper'

RSpec.describe 'potepan::categories', type: :request do
  describe 'potepan/categories' do
    let(:taxonomy)  { create(:taxonomy, name: 'categories') }
    let(:taxon1)    { create(:taxon, name: 'Bags', taxonomy: taxonomy) }
    let(:taxon2)    { create(:taxon, name: 'Mugs', taxonomy: taxonomy) }
    let!(:product)  { create(:product, name: 'TOTE', taxons: [taxon1]) }
    let!(:product2) { create(:product, name: 'Ruby', taxons: [taxon2]) }

    before do
      get potepan_category_path(taxon1.id)
    end

    it 'responds successfully' do
      expect(response).to be_successful
    end

    it 'include categories' do
      expect(response.body).to include taxonomy.name
    end

    it 'include taxon' do
      expect(response.body).to include taxon1.name
    end

    it 'include product' do
      expect(response.body).to include product.name
    end

    it 'not include different catagories product' do
      expect(response.body).not_to include product2.name
    end
  end
end
