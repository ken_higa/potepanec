require 'rails_helper'

RSpec.describe 'Potepan::Products template', type: :request do
  describe 'potepan/products' do
    let(:product) do
      FactoryBot.create(:product, name: "Ruby example",
                                  price: "$22.00", description: "description",
                                  taxons: [taxon])
    end

    let(:taxon) { create(:taxon) }

    before do
      get potepan_product_path(product.id)
    end

    it 'responds successfully' do
      expect(response).to be_successful
      expect(response).to have_http_status '200'
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end

    it 'show template product name' do
      expect(response.body).to include product.name
    end

    it 'show teamplate product price' do
      expect(response.body).to include product.price.to_s
    end

    it 'show template product description' do
      expect(response.body).to include product.description
    end
  end
end
